$(document).ready(() => {
  if (localStorage.getItem("expired") < Date.now()) {
    localStorage.clear();
  }
  const username = localStorage.getItem("username");
  const namaLengkap = localStorage.getItem("namaLengkap");

  if (username != null) {
    $(".auth-stuff").remove();
    let dropdownContent =
      `<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">` +
      namaLengkap +
      `</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" id="logout-button" href="#">Logout</a>
        </div>
        </li>`;

    $(".drop-profile").append(dropdownContent);
  }

  $("#logout-button").click(() => {
    localStorage.clear();
    window.location = "/login";
  });
});
