from unicodedata import name
from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.index, name='index'),
    path('detail/<int:idBlog>', views.detail_blog, name='detailBlog'),
    path('login', views.auth, name='login'),
    path('register', views.register, name='register'),
    path('create_blog', views.create_blog, name='createBlog'),
    path('update_blog/<int:idBlog>', views.update_blog, name='updateBlog'),
    path('about', views.profile, name='profile')
]