from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'blog/main.html')

def detail_blog(request,idBlog):
    return render(request,'blog/detail_blog.html')

def auth(request):
    return render(request, 'blog/login.html')

def register(request):
    return render(request, 'blog/register.html')

def create_blog(request):
    return render(request, 'blog/create_blog.html')

def update_blog(request, idBlog):
    return render(request, 'blog/update_blog.html')

def profile(request):
    return render(request, 'blog/profile.html')