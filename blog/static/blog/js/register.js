$("#register-button").click(function (e) { 
    let url;
    if($(location).attr('href').includes("127.0.0.1")) {
        url = "http://localhost:3000";
    } else {
        url = "https://blogapp-api-arya.herokuapp.com";
    }
    
    let redirectUrl;
    if($(location).attr('href').includes("127.0.0.1")) {
        redirectUrl = "http://127.0.0.1:8000";
    } else {
        redirectUrl = "https://blogapp-arya.herokuapp.com";
    } 

    const newUser = {
        namaLengkap : $('#namaLengkap').val(),
        username : $('#username').val(),
        password : $('#password').val()
    }

    $.ajax({
        type: "POST",
        url: url + '/user',
        contentType: 'application/json',
        data: JSON.stringify(newUser),
        dataType:"json",
        success: function (response) {
            alert('Register success!!! Please Login ❤️')

            window.location = redirectUrl + '/login';
        },
        error : () => {
            alert('Username sudah digunakan')
        }
    });
});