let link;
link = window.location.href;
const index = link.indexOf("update_blog/")
const idBlog = link.slice(index + 12);

let token = localStorage.getItem("tokenRes");

let url;
if($(location).attr('href').includes("127.0.0.1")) {
    url = "http://localhost:3000";
} else {
    url = "https://blogapp-api-arya.herokuapp.com";
}

let redirectUrl;
if($(location).attr('href').includes("127.0.0.1")) {
    redirectUrl = "http://127.0.0.1:8000";
} else {
    redirectUrl = "https://blogapp-arya.herokuapp.com";
}

$.ajax({
    type: "GET",
    url: url + '/blog/' + idBlog    ,
    dataType: "json",
    contentType : 'application/json',
    success: function (response) {
        $("#blog-title").val(response.blogTitle)
        $("#blog-content").val(response.blogContent)
    }
});

$("#update-button").click(() => {
    const dataBlog = {
        blogTitle: $('#blog-title').val(),
        blogContent:$('#blog-content').val()
    }

    $.ajax({
        type: "PUT",
        url: url + '/blog/' + idBlog    ,
        dataType: "json",
        contentType : 'application/json',
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + token,
        },
        success: function (response) {
            alert("Berhasil Update Blog");
            window.location = redirectUrl;
        },
        data: JSON.stringify(dataBlog)
    });
});