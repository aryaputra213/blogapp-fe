$("#login-button").click(function (e) { 
    let url;
    if($(location).attr('href').includes("127.0.0.1")) {
        url = "http://localhost:3000";
    } else {
        url = "https://blogapp-api-arya.herokuapp.com";
    }

    let redirectUrl;
    if($(location).attr('href').includes("127.0.0.1")) {
        redirectUrl = "http://127.0.0.1:8000";
    } else {
        redirectUrl = "https://blogapp-arya.herokuapp.com";
    } 
    const authData = {
        username : $("#username").val(),
        password : $("#password").val()
    }

    $.ajax({
        type: "POST",
        url: url + '/auth/login',
        contentType: 'application/json',
        data: JSON.stringify(authData),
        dataType:"json",
        success: function (response) {
            localStorage.setItem('expired', Date.now() + 1800000);
            localStorage.setItem('id', response.id);
            localStorage.setItem('tokenRes', response.access_token);
            localStorage.setItem('namaLengkap', response.namaLengkap);
            localStorage.setItem('username', response.username);
            localStorage.setItem('role', response.role)

            window.location = redirectUrl;
        },
        error : () => {
            alert('wrong password')
        }
    });
});