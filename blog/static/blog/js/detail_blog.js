let link;
link = window.location.href;
const index = link.indexOf("detail/")
const idBlog = link.slice(index + 7);

const token = localStorage.getItem("tokenRes");
const namaLengkap = localStorage.getItem("namaLengkap");
const username = localStorage.getItem("username");

let url;
if($(location).attr('href').includes("127.0.0.1")) {
    url = "http://localhost:3000";
} else {
    url = "https://blogapp-api-arya.herokuapp.com";
}

let redirectUrl;
if($(location).attr('href').includes("127.0.0.1")) {
    redirectUrl = "http://127.0.0.1:8000";
} else {
    redirectUrl = "https://blogapp-arya.herokuapp.com";
}

$.ajax({
    type: "GET",
    url: url + '/blog/' + idBlog    ,
    dataType: "json",
    contentType : 'application/json',
    success: function (response) {
        $('.blog-data').append(`
        <div id="title">
        <h1>` + response.blogTitle + `</h1>
        <hr>
      </div>
      <div class="card">
          <div class="card-body">
              <p>` + response.blogContent +`</p>
          </div>
      </div>
        `)

        for (var i = 0 ; i < Object.keys(response.komentars).length ; i++) {
            $.ajax({
                type: "GET",
                url: url + '/komentar/'+response.komentars[i].id,
                dataType: "json",
                contentType : 'application/json',
                success: function (responseKomen) {
                    if (responseKomen.user.username === username) {
                        $('.list-komentar').append(
                            `<div class="komentar">
                            <strong>`+ responseKomen.user.namaLengkap + `</strong>
                            <p id="` + responseKomen.id  + `-komentar">` + responseKomen.komentarText + `</p>
                            <div>
                                <a id="` + responseKomen.id  + `" onClick = "updateButton(this.id)" class="btn btn-success">Update Komentar</a>
                                <a id="` + responseKomen.id  + `" onClick = "deleteButton(this.id)" class="btn btn-danger">Delete Komentar</a>
                            </div>
                        </div>`
                        );
                    } else {
                        $('.list-komentar').append(
                            `<div class="komentar">
                            <strong>`+ responseKomen.user.namaLengkap + `</strong>
                            <p id="` + responseKomen.id  + `-komentar">` + responseKomen.komentarText + `</p>
                        </div>`
                        );

                    }
                }
            });
        }
    }
});




$('#komentar-content').click(function () { 
    if (token != null) {
        if ($("#komentar-area").val() != '') {
            const komentar = {
                komentarText :  $("#komentar-area").val()
            }
            $("#komentar-area").val('')
            const idUser = localStorage.getItem('id');
        
            $.ajax({
                type: "POST",
                url: url + '/komentar/create/' + idUser + '/' + idBlog   ,
                contentType: 'application/json',
                data: JSON.stringify(komentar),
                dataType:"json",
                headers: {
                    "Access-Control-Allow-Origin": "*",
                    "Authorization": "Bearer " + token,
                },
                success: function (response) {
                    $('.list-komentar').append(
                        `<div class="komentar">
                        <strong>`+ response.user.namaLengkap + `</strong>
                        <p id="` + response.id  + `-komentar">` + response.komentarText + `</p>
                        <div>
                            <a id="` + response.id  + `" onClick = "updateButton(this.id)" class="btn btn-success">Update Komentar</a>
                            <a id="` + response.id  + `" onClick = "deleteButton(this.id)" class="btn btn-danger">Delete Komentar</a>
                        </div>
                    </div>`
                    );
                }
            });
        } else {
            alert('No comment there');
        }
    } else {
        alert('You must login!')
        window.location = redirectUrl + '/login';
    }
});

function updateButton(idKomentar) {
    const idKom = idKomentar+'-komentar';
    const komentar = document.getElementById(idKom).textContent;
    $("#"+idKom).replaceWith(`<input style="margin-top: 5px; margin-bottom: 5px;" id="newKomen" class="form-control" placeholder="Your new comment">`);
    $("#newKomen").val(komentar);
    const idkom2 = '#' + idKomentar;
    $(idkom2).replaceWith(`<a id="` + idKomentar  + `" onClick = "saveButton(this.id)" class="btn btn-success">Save Komentar</a>`)
    
  }

      
function saveButton(idKomentar) {
    const komentar = {
        komentarText : $("#newKomen").val()
    }

    $.ajax({
        type: "PUT",
        url: url + '/komentar/'+idKomentar,
        contentType: 'application/json',
        data: JSON.stringify(komentar),
        dataType:"json",
         headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + token,
        },
        success: function (response) {
            alert('Update successfully')
            location.reload()
        }
    });
}

function deleteButton(idKomentar) {
    $.ajax({
        type: "DELETE",
        url: url + '/komentar/'+idKomentar,
        contentType: 'application/json',
         headers: {
            "Access-Control-Allow-Origin": "*",
            "Authorization": "Bearer " + token,
        },
        success: function (response) {
            alert('Delete successfully')
            location.reload()
        }
    });

}