
  const role = localStorage.getItem("role");
  const token = localStorage.getItem("tokenRes");

  let url;
  if($(location).attr('href').includes("127.0.0.1")) {
      url = "http://localhost:3000";
  } else {
      url = "https://blogapp-api-arya.herokuapp.com";
  }

  let redirectUrl;
  if($(location).attr('href').includes("127.0.0.1")) {
      redirectUrl = "http://127.0.0.1:8000";
  } else {
      redirectUrl = "https://blogapp-arya.herokuapp.com";
  }

  if (role === "PENULIS") {
    $(".penulis-related-createblog").append(
      `<a id="blog-creator" href="/create_blog" class="btn btn-primary">Create New Blog</a>`
    );

    $(".penulis-related-updateBlog").append(
      `<a href="{% url 'blog:detailBlog' %}" class="btn btn-success">Update Blog</a>`
    );
  }

  $.ajax({
    type: "GET",
    url: url + '/blog',
    dataType: "json",
    contentType : 'application/json',
    success: function (response) {
      if (role === "PENULIS") {
        for (var i = 0 ; i < Object.keys(response).length ; i++) {
          $('.list-blog').append(
            `<div class="card">
            <div class="card-body">
              <h3>` +response[i].blogTitle +  `</h3>
              <p>By: Aryaputra Athallah</p>
              <p>` +
                response[i].blogContent.substring(0,1000)+`...`
              +`</p>
              <div class="penulis-related-updateBlog">
                <a id="` + response[i].idBlog + `"  onClick = "readButton(this.id)" class="btn btn-primary">Read More</a>
                <a id="` + response[i].idBlog  + `" onClick = "updateButton(this.id)" class="btn btn-success">Update Blog</a>
                <a id="` + response[i].idBlog  + `" onClick = "deleteButton(this.id)" class="btn btn-danger">Delete Blog</a>
              </div>
            </div>
          </div>`
          )
        }
      } else {
        for (var j = 0 ; j < Object.keys(response).length ; j++) {
          $('.list-blog').append(
            `<div class="card">
            <div class="card-body">
              <h3>` +response[j].blogTitle +  `</h3>
              <p>By: Aryaputra Athallah</p>
              <p>` +
                response[j].blogContent.substring(0,1000)+`...`
              +`</p>
              <div class="penulis-related-updateBlog">
              <a id="` + response[j].idBlog + `"  onClick = "readButton(this.id)" class="btn btn-primary">Read More</a>
              </div>
            </div>
          </div>`
          )
        }
      }
    }
  });
  function updateButton(idBlog) {
    window.location = redirectUrl + "/update_blog/" + idBlog;
  }

  function readButton(idBlog) {
    window.location = redirectUrl + "/detail/" + idBlog;
  }

  function deleteButton(idBlog) {
    $.ajax({
      type: "DELETE",
      url: url + '/blog/' + idBlog,
      dataType: "json",
      contentType: 'application/json',
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Authorization": "Bearer " + token,
    },
      success: function (response) {
        alert('Delete successfully');
        location.reload();
      }
    });
  }



