$('#create-button').click(function (e) { 
    const token = localStorage.getItem("tokenRes");
    const id = localStorage.getItem("id");
    
    let url;
    if($(location).attr('href').includes("127.0.0.1")) {
        url = "http://localhost:3000";
    } else {
        url = "https://blogapp-api-arya.herokuapp.com";
    }
    
    let redirectUrl;
    if($(location).attr('href').includes("127.0.0.1")) {
        redirectUrl = "http://127.0.0.1:8000";
    } else {
        redirectUrl = "https://blogapp-arya.herokuapp.com";
    }
  
    if ($('#blog-title').val() != '' && $('#blog-content').val() != '' ) {
        const dataBlog = {
            blogTitle: $('#blog-title').val(),
            blogContent:$('#blog-content').val()
        }
    
    
        $.ajax({
            type: "POST",
            url: url + '/blog/' + id,
            contentType: 'application/json',
            data: JSON.stringify(dataBlog),
            dataType:"json",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Authorization": "Bearer " + token,
            },
            success: function (response) {
                alert('successfull 🔥🔥🔥');
                window.location = redirectUrl;
            }
        });
    } 
});